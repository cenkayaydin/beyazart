const gulp = require('gulp'); // gulp import edildi.
const cleanCss = require('gulp-cleancss'); // cssleri küçültme paketi import edildi
const browserSync = require('browser-sync').create(); // browser bağlantısı import edildi.
const imageMin = require('gulp-imagemin');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify'); // js küçültme paketi
const concat = require('gulp-concat'); // dosyaları bir araya getirlme paketi

gulp .task('html', () => {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream())
});

gulp.task('css', () => {
    gulp.src('./src/styles/**/*.css')
        .pipe(cleanCss())
        .pipe(gulp.dest('./dist/styles/'))
        .pipe(browserSync.stream())
});

gulp.task('sass', () => {
    gulp.src('./src/styles/**/style.scss') // bu dosyayı veya dosyaları derle
        .pipe(sass()) // sass dosyalarını css e çevir
        .pipe(gulp.dest('./dist/styles/')) // sass dosyalrını bu dizin altına koy
        .pipe(browserSync.stream()); // browser bağlantısı yap
});

gulp.task('Jscon', () => {
    gulp.src('./src/scripts/**/*.js') // bu dosyayı veya dosyaları derle
        .pipe(concat('all.js')) // js dosyalrını bu isimde tek dosya haline getir
        .pipe(uglify()) // js dosyalarını küçült
        .pipe(gulp.dest('./dist/scripts/')) // bu dizine koy
        .pipe(browserSync.stream()); // browser bağlantısını yap
});

gulp.task('image', () => {
    gulp.src('./src/images/**/*')
        .pipe(imageMin([
            imageMin.jpegtran({progressive: true}), // jpg dosyası küçültme
            imageMin.optipng(), // png dosyasu için
            imageMin.gifsicle({interlaced: true}) // gif için
        ]))
        .pipe(gulp.dest('./dist/images/'))
        .pipe(browserSync.stream());
});

gulp.task('watch', ['html', 'css', 'image', 'sass', 'Jscon'], () => {
    browserSync.init({
        server: './dist/'
    });
    gulp.watch('./src/styles/**/*.scss', ['sass']);
    gulp.watch('./src/styles/**/*.css', ['css']);
    gulp.watch('./src/scripts/**/*.js', ['Jscon']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/images/**/*', ['image'])
});

gulp.task('default', ['watch']);